//
//  DataManagerMock.swift
//  ProPlusNalogaTests
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import Foundation
import ObjectMapper
import ReactiveSwift

class MainDataManagerMock: DataManagerProtocol {
    
    func fetchNewsPosts() -> SignalProducer<[Post], NSError> {

        return SignalProducer{ observer,_ in
            
            guard let newsResponse = self.readFile() else{
                fatalError("Mock should return a dictionary!")
            }
            
            guard let data = newsResponse["data"] as? [String:Any] else{
                fatalError("response should have a data key in a dictionary!")
            }
            
            guard let front = data["front"] as? [String:Any] else{
                fatalError("data should have a front key in a dictionary!")
            }
            
            if let posts = Mapper<Post>().mapArray(JSONObject: front["articles"]) {
                
                observer.send(value: posts)
            }
            observer.sendCompleted()
        }
    }
    
    func readFile() -> [String: Any]? {
        let testBundle = Bundle(for: type(of: self))
        if let path = testBundle.path(forResource: "result", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                let jsonResult = try JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                if let jsonResult = jsonResult as? [String: Any]{
                    return jsonResult
                }
            } catch let error {
                return nil
            }
        }
        return nil
    }
}
