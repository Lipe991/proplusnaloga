//
//  ProPlusNalogaTests.swift
//  ProPlusNalogaTests
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import XCTest
@testable import ProPlusNaloga

class ProPlusNalogaTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testDataManagerMapping() {
        let network = MainDataManagerMock()
        network.fetchNewsPosts()
            .on(value: { posts in
                XCTAssertTrue(posts.count == 1)
                if let post = posts.first {
                    XCTAssertTrue(post.title == "(Prazna) volilna kampanja: Janša si je nadel prijazen obraz, SD je poskrbela za največji ‘kiks’")
                    XCTAssertTrue(post.frontImage == "https://images.24ur.com/media/images/PLACEHOLDER/May2018/2e770e868f_62084592.jpg?v=d41d")
                } else {
                    fatalError("Should be at least one post in array!")
                }

            }).start()
    }
    
}
