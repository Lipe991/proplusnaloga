//
//  MainEndpoint.swift
//  ProPlusNaloga
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import Foundation

struct API {
    static let baseUrl = "http://oglasi-gql.24ur.si"
}

protocol Endpoint {
    var posts: String { get }
    var params: [String: String] { get }
    var headers: [String: String] { get }
    var url: String { get }
}

enum Endpoints {
    
    enum News: Endpoint {
        case fetch
        
        public var posts: String {
            switch self {
            case .fetch: return "/news"
            }
        }
        
        public var headers: [String : String] {
            switch self {
            case .fetch: return ["Content-Type": "application/json"]
            }
        }
        
        public var params: [String : String] {
            switch self {
            case .fetch: return ["query": "{front(sectionId:1,page:1){id,sectionId,url,articles{id,title,date,frontImage{id,src},style}}}",
                                 "raw": ""]
            }
        }
        
        public var url: String {
            switch self {
            case .fetch: return "\(API.baseUrl)\(posts)"
            }
        }
    }
}
