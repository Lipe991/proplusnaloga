//
//  MainController.swift
//  ProPlusNaloga
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import Foundation
import RenderNeutrino
import ReactiveSwift

class MainController: UITableComponentViewController {
    
    var posts = [Post]()
    var task: DispatchWorkItem?
    var postObserver = CompositeDisposable()
    
    let dataManager = MainDataManager()
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return posts.count > 0 ? 10000 : 0
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let props = MainCellProps()
        props.post = self.posts[indexPath.row % posts.count]
        let component = context.component(MainTableCellComponent.self,
                                          key: "postId",
                                          props: props,
                                          parent: nil)
        return dequeueCell(forComponent: component)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UIScreen.main.bounds.height
    }
    
    override func viewDidLoad() {
        self.view.backgroundColor = .white
        self.tableView.isPagingEnabled = true
        self.tableView.showsHorizontalScrollIndicator = false
        self.tableView.showsVerticalScrollIndicator = false
        super.viewDidLoad()
        

        postObserver += dataManager.fetchNewsPosts()
        .on(value: { [weak self] posts in
            guard let this = self else { return }
            this.posts = posts
            this.reloadData()
        }).start()
        
        setupTimer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.postObserver.dispose()
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        setupTimer()
    }
    
    func scrollViewDidEndScrollingAnimation(_ scrollView: UIScrollView) {
        setupTimer()
    }
    
    func setupTimer() {
        self.task?.cancel()
        self.task = DispatchWorkItem { [weak self] in
            guard let this = self else { return }
            if let indexPath = this.tableView.indexPathsForVisibleRows?.first {
                this.tableView.scrollToRow(at: IndexPath(row: indexPath.row + 1, section: 0), at: UITableViewScrollPosition.top, animated: true)
            }
        }
        
        if let task = self.task {
            DispatchQueue.main.asyncAfter(deadline: .now() + 5, execute: task)
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        self.tableView.reloadData()
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }

}
