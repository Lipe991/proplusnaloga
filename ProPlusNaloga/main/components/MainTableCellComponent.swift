//
//  MainTableCellComponent.swift
//  ProPlusNaloga
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import RenderNeutrino
import Nuke

class MainCellProps: UIProps {
    var post: Post?
    required init(){}
    required init(post: Post) {
        self.post = post
    }
}

class PostHelper{
    static func generateImage(post: Post?) -> URL? {
        return URL(string: post?.frontImage.replacingOccurrences(of: "PLACEHOLDER", with: "\(Int(UIScreen.main.bounds.width))x\(Int(UIScreen.main.bounds.height))") ?? "")
    }
}

class MainTableCellComponent: UIComponent<UINilState,MainCellProps> {
    override func render(context: UIContextProtocol) -> UINodeProtocol {
        return wrapper().children([
            image(url: PostHelper.generateImage(post: props.post)).children([
                title(title: props.post?.title)
            ])
        ])
    }
}

fileprivate func wrapper() -> UINode<UIView> {
    return UINode<UIView>(){specs in
        specs.view.yoga.width = specs.canvasSize.width
    }
}

fileprivate func title(title: String?) -> UINode<UIView> {
    let label = UINode<UILabel>(){specs in
        specs.view.text = title
        specs.view.font = UIFont.boldSystemFont(ofSize: 32)
        specs.view.textColor = UIColor.white
        specs.view.yoga.flex()
        specs.view.numberOfLines = 0
    }
    
    let holderBg = UINode<UIView>(){specs in
        specs.view.yoga.position = .absolute
        specs.view.yoga.width = specs.canvasSize.width
        specs.view.yoga.left = 0
        specs.view.yoga.bottom = 0
        specs.view.yoga.paddingTop = 10
        specs.view.yoga.paddingLeft = 10
        specs.view.yoga.paddingBottom = 10
        specs.view.yoga.paddingRight = 10
        specs.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
    }
    
    holderBg.children([label])
    
    return holderBg
}

fileprivate func image(url: URL?) -> UINode<UIImageView> {
    return UINode<UIImageView>(){specs in
        specs.view.yoga.width = UIScreen.main.bounds.width
        specs.view.yoga.height = UIScreen.main.bounds.height
        
        if let url = url {
            Manager.shared.loadImage(with: url, into: specs.view)
        }
    }
}
