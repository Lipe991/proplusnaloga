//
//  MainDataManager.swift
//  ProPlusNaloga
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import Foundation
import Alamofire
import ReactiveSwift
import ObjectMapper

class MainDataManager: DataManagerProtocol {
    func fetchNewsPosts() -> SignalProducer<[Post], NSError> {
        return SignalProducer{ observer,_ in
            Alamofire.request(Endpoints.News.fetch.url, method: .get, parameters: Endpoints.News.fetch.params)
                .responseJSON(completionHandler: { (response) in
                    guard let newsResponse = response.result.value as? [String:Any] else{
                        return
                    }
                    
                    guard let data = newsResponse["data"] as? [String:Any] else{
                        return
                    }
                    
                    guard let front = data["front"] as? [String:Any] else{
                        return
                    }
                    
                    if let posts = Mapper<Post>().mapArray(JSONObject: front["articles"]) {
                        
                        observer.send(value: posts)
                    }
                    observer.sendCompleted()
                })
        }.start(on: QueueScheduler()).observe(on: UIScheduler())
    }
}
