//
//  DataManagerProtocol.swift
//  ProPlusNaloga
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol DataManagerProtocol {
    func fetchNewsPosts() -> SignalProducer<[Post], NSError>
}
