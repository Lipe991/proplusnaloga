//
//  Post.swift
//  ProPlusNaloga
//
//  Created by Sandi Mihelic on 28/05/2018.
//  Copyright © 2018 Sandi Mihelic. All rights reserved.
//

import Foundation
import ObjectMapper

struct Post: Mappable {
    var id = 0
    var title = ""
    var date = 0
    var frontImage = ""
    var style = ""
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        title <- map["title"]
        date <- map["date"]
        frontImage <- map["frontImage.src"]
        style <- map["style"]
    }
}
